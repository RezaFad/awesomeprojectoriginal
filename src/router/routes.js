const routes = [
  {
    path: '/blog',
    component: () => import('layouts/Layout.vue'),
    children: [
      { path: '/blog', name: 'pagebook', component: () => import('pages/PageBlog.vue') },
      { path: '/addBlog', name: 'addbook', component: () => import('pages/PageAddBlog.vue') },
      { path: '/detail/:id', name: 'detail', component: () => import('pages/PageDetail.vue') },
      { path: '/update/:id', name: 'update', component: () => import('pages/PageUpdateBlog.vue') },
      { path: '/employee', name: 'employee', component: () => import('pages/Employee/View.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  },

  {
    path: '/',
    name: 'login',
    component: () => import('pages/Login.vue')
  },

  {
    path: '/register',
    name: 'register',
    component: () => import('pages/Register')
  }
]

export default routes
